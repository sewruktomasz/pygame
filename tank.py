import pygame
from pygame.math import Vector2
from bullet import *

class Tank(object):

    def __init__(self, game):
        self.game = game

        self.window_size = self.game.screen.get_size()

        self.size = Vector2(40,50)
        self.pos = Vector2((self.window_size[0]-self.size.x/2)/2,(self.window_size[1]+self.size.y/2)/2)
        self.vel = Vector2(0,0)
        self.acc = Vector2(0,0)
        self.speed = 1.0
        self.speed_copy = self.speed
        self.sizeOfTank = 10





    def add_force(self, force):
        self.acc += force




    def tick(self):
        #Poruszanie
        keys = pygame.key.get_pressed()
        keys2 = pygame.key.get_repeat()
        if (keys[pygame.K_d] or keys[pygame.K_RIGHT]) and self.pos.x < self.window_size[0] - self.size.x : #self.window_size[0] + self.size.x:
            self.add_force(Vector2(self.speed,0))
        if (keys[pygame.K_a] or keys[pygame.K_LEFT]) and self.pos.x > 0 + self.size.x:
            self.add_force(Vector2(-self.speed,0))
        if (keys[pygame.K_w] or keys[pygame.K_UP]) and self.pos.y > 0 + self.size.y:
            self.add_force(Vector2(0,-self.speed))
        if (keys[pygame.K_s] or keys[pygame.K_DOWN]) and self.pos.y < self.window_size[1] - self.size.y:
            self.add_force(Vector2(0,self.speed))
        if keys[pygame.K_SPACE]:
            pass
        if True:
            if keys[pygame.K_z]:
                self.speed *=1.01
            else:
                self.speed = self.speed_copy
        if keys[pygame.K_x]:
            self.speed = 4





        #Fizyka
        self.vel -= Vector2(0,0) #grawitacja np Vector2(0,-0.5)
        self.vel *= 0.8 #poslizg
        self.vel += 0.8*self.acc
        self.pos += self.vel
        self.acc *= 0.0
        


    def draw(self):
        #Pozycja bazowa
        points = [Vector2(-3,-3), Vector2(-1,-3), Vector2(-1,-1), Vector2(1,-1),Vector2(1,-3),Vector2(3,-3),Vector2(3,1),Vector2(1,1),Vector2(1,3),Vector2(-1,3),Vector2(-1,1),Vector2(-3,1)]

        #Obracania
        angle = self.vel.angle_to(Vector2(0,1))
        points = [p.rotate(angle) for p in points]

        #Naprawa osi x
        points = [Vector2(p.x*-1, p.y) for p in points]

        #Dodawanie obecnej pozycji
        points = [Vector2(self.pos+p*self.sizeOfTank) for p in points]

        #Rysowanie
        pygame.draw.polygon(self.game.screen, (38, 71, 35), points)

        #rect = pygame.Rect(self.pos.x, self.pos.y, self.size.x, self.size.y)
        #pygame.draw.rect(self.game.screen, (169, 169, 169), rect, 0)