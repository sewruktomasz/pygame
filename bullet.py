import pygame
from pygame.math import Vector2

class Bullet(object):

    def __init__(self, game):
        self.game = game

        self.window_size = self.game.screen.get_size()

        self.size = Vector2(40,50)
        self.pos = Vector2((self.window_size[0]-self.size.x/2)/2,(self.window_size[1]+self.size.y/2)/2)
        self.vel = Vector2(0,0)
        self.acc = Vector2(0,0)
        self.speed = 1.5
        self.sizeOfBullet = 2



    def add_force(self, force):
        self.acc += force




    def tick(self):
        #Poruszanie




        #Fizyka
        self.vel -= Vector2(0,0) #grawitacja Vector2(0,-0.5)
        self.vel *= 0.8 #poslizg
        self.vel += self.acc
        self.pos += self.vel
        self.acc *= 0


    def draw(self):
        #Pozycja bazowa
        points = [Vector2(-1,-1), Vector2(0,-1), Vector2(0,0), Vector2(1,0)]

        #Obracania
        angle = self.vel.angle_to(Vector2(0,1))
        points = [p.rotate(angle) for p in points]

        #Naprawa osi x
        points = [Vector2(p.x*-1, p.y) for p in points]

        #Dodawanie obecnej pozycji
        points = [Vector2(self.pos+p*self.sizeOfBullet) for p in points]

        #Rysowanie
        pygame.draw.polygon(self.game.screen, (169, 171, 135), points)

        #rect = pygame.Rect(self.pos.x, self.pos.y, self.size.x, self.size.y)
        #pygame.draw.rect(self.game.screen, (169, 169, 169), rect, 0)