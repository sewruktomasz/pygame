import pygame, sys
from tank import *


class Game(object):



    def __init__(self):
        #Konfiguracja
        self.tps_max = 30.0
        self.bg_img = pygame.image.load('./files/images/bg.jpg')


        #Inicjalizacja
        pygame.init()



        self.screen = pygame.display.set_mode((500,500))
        self.tps_clock  = pygame.time.Clock()
        self.tps_delta  = 0.0

        self.player = Tank(self)

        while (True):

            # Wydarzenia
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit(0)
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    sys.exit(0)

              # Wladza nad czasem
            self.tps_delta += self.tps_clock.tick() / 1000.0
            while (self.tps_delta > 1 / self.tps_max):
                self.tick()
                self.tps_delta -= 1 / self.tps_max


            #Rysowanie

            self.screen.blit(self.bg_img, self.bg_img.get_rect())
            self.draw()
            pygame.display.flip()

    def tick(self):
        # Sprawdzanie wejscia
        self.player.tick()


    def draw(self):
        # Wyswietlanie
        myfont = pygame.font.SysFont("monospace", 25)
        myfont_small = pygame.font.SysFont("Arial", 15)

        level = myfont.render("Level 1", 1, (255, 255, 0))
        self.screen.blit(level, (5, 5))

        speed = myfont.render("speed = "+str(round(self.player.speed,2)), 1, (255, 255, 0))
        self.screen.blit(speed, (150, 150))

        position = myfont_small.render("x="+str(round(self.player.pos.x,1))+" y="+str(round(self.player.pos.y,1)), 1, (10, 0, 100))
        self.screen.blit(position, (5, 50))

        fps = myfont_small.render(str(self.tps_clock), 1, (10, 0, 100))
        self.screen.blit(fps, (5, 100))

        window_size = myfont.render(str(round(self.player.vel.x*self.player.vel.y,2)), 1, (255, 255, 0))
        self.screen.blit(window_size, (5, 470))





        self.player.draw()





if __name__ == "__main__":
    Game()
















